package com.example.rent.sda_045_pb_service_app;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.example.rent.sda_045_pb_service_app.MainActivity.INTENT_KEY;

/**
 * Created by RENT on 2017-06-12.
 */

public class TimerService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int inputNumber = intent.getIntExtra(INTENT_KEY, 5);

        // 1 sposob - przytnie bo zatrzymuje glowny watek
//        for (int i = inputNumber; i > 0; i-- ) {
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        // 2 sposob - klasa wbudowana counter
        final long number = (intent.getLongExtra("number",0))*1000;

        new CountDownTimer(number, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                String counter = String.valueOf(millisUntilFinished/1000);
                Log.i("counter",counter );
            }

            @Override
            public void onFinish() {
                Log.i("counter","Times up!" );
            }
        }.start();

        return START_NOT_STICKY;
    }
}
