package com.example.rent.sda_045_pb_service_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String INTENT_KEY = "key";

    @BindView(R.id.edit_text_input_number)
    protected EditText editTextInputNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_start)
    protected void clickStartButton(){
        String textNumber = editTextInputNumber.getText().toString();
        long number = Long.valueOf(textNumber);
        Intent intent = new Intent(this, TimerService.class);
        intent.putExtra("number", number);
        startService(intent);

    }
}
